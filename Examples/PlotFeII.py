import matplotlib.pyplot as plt
import scipy
import VoigtFitting.VoigtFitting as VoigtFitting
import time
#import AbsorptionLine, ResolutionConvolution,FitManager


Fit = VoigtFitting.FitManager()

#read data:

Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '9623',9623.0, 9657.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '13461',13461.0, 13491.0,1.1)
Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '15476',15476.0, 15505.0,1.43)
Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '15556',15556.0, 15584.0,1.43)

Ion = Fit.CreateIonGroup('FeII',['FeII_1608','FeII_1611','FeII_2249','FeII_2586','FeII_2600'])
Ion.AddComponent('A',2.0e15, 1.2e6,4.9887)
Ion.AddComponent('B',1.0e15, 1.2e6,4.9890)
Ion.AddComponent('C',10.0e15, 1.2e6,4.9892)
Ion.AddComponent('D',1.0e15, 1.2e6,4.9894)
#Ion.AddComponent('B',1.0e15, 2.2e6,4.9895)
#Ion.AddComponent('C',2.0e15, 2.2e6,4.9885)
#Ion.AddComponent('B',6.0e16, 300000,4.99)

    





Arrow = lambda text,posx,col: plt.annotate(text, xy=(  posx, 1.2), xytext=(posx,1.8),fontsize=10,ha='center',arrowprops=dict(facecolor=col, shrink=0.2))
Nx,Ny = Fit.PlotChunks()




ChunkKeys = Fit.GetDataChunk().keys()
SortedKeys = []
for i in ChunkKeys:

    SortedKeys.append(int(i))

SortedKeys.sort()

plt.subplot(Nx,Ny,1)

Arrow('FeII1608', 9633.5 ,'blue')
Arrow('FeII1611', 9649.5,'blue')

ThisChunk = Fit.GetDataChunk(str(SortedKeys[0]))
x = scipy.arange(ThisChunk.GetLambdaMin(),ThisChunk.GetLambdaMax(),0.1)
y = Fit.Convolve(ThisChunk.Resolution,x)
plt.plot(x,y,'-',lw=2,color='black')
  

plt.subplot(Nx,Ny,2)
Arrow('FeII2249', 13475.0  ,'blue')

ThisChunk = Fit.GetDataChunk(str(SortedKeys[1]))
x = scipy.arange(ThisChunk.GetLambdaMin(),ThisChunk.GetLambdaMax(),0.1)
y = Fit.Convolve(ThisChunk.Resolution,x)
plt.plot(x,y,'-',lw=2,color='black')

plt.subplot(Nx,Ny,3)
Arrow('FeII2586', 15491.0  ,'blue')


ThisChunk = Fit.GetDataChunk(str(SortedKeys[2]))
x = scipy.arange(ThisChunk.GetLambdaMin(),ThisChunk.GetLambdaMax(),0.1)
y = Fit.Convolve(ThisChunk.Resolution,x)
plt.plot(x,y,'-',lw=2,color='black')

plt.subplot(Nx,Ny,4)
Arrow('FeII2599', 15570.0  ,'blue')

ThisChunk = Fit.GetDataChunk(str(SortedKeys[3]))
x = scipy.arange(ThisChunk.GetLambdaMin(),ThisChunk.GetLambdaMax(),0.1)
y = Fit.Convolve(ThisChunk.Resolution,x)
plt.plot(x,y,'-',lw=2,color='black')



plt.show()