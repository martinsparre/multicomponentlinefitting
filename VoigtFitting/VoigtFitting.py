from math import pi
import scipy,scipy.signal
import time,commands
import LineDict.Pickle_functions as Pickle_functions
import random
import matplotlib.pyplot as plt
import pylab
import sys,copy
import emcee,numpy
import pdb
#functions


def SetLabels(xsize=24,ysize=24):
    "Set plot (matplotlib) label fontsize."
    ax = pylab.gca()
    for tick in ax.xaxis.get_major_ticks():
        tick.label1.set_fontsize(xsize)

    for tick in ax.yaxis.get_major_ticks():
        tick.label1.set_fontsize(ysize)

def Gauss(x,s,mu=0):
    return scipy.exp(-(x-mu)**2.0/(2.0*s**2))

    
def ResolutionConvolution(resolution,wav,flux):
    """
    if resolution is a number:
    Convolves with a gaussian with a given resolution. resolution is sigma (not FWHM).
    
    if resolution is a string:
    Convolves with an arbitrary instrument profile specified in the file Filename (angstrom,profile). Requirements: x-interval must be symmetric. Maximum must be at x=0
    """
    wav = copy.deepcopy(wav)
    wavmax = wav[-1]
    wavmin = wav[0]
    flux = copy.deepcopy(flux)


    if type(resolution) is float:
        cdeltx = wav[1]-wav[0]
        x1 = scipy.arange(-6*resolution,6*resolution,cdeltx)
        y1 = Gauss(x1,resolution)
        #print y1
        y1/= scipy.integrate.trapz(y1,x1)
        
        OnePadding = True
        if OnePadding:#avoid drop in model spectrum near endpoints...
            x_add_left = scipy.arange(wav[0]-50.0*(wav[1]-wav[0]),wav[0],wav[1]-wav[0])
            x_add_right = scipy.arange(wav[-1]+(wav[-1]-wav[-2]),wav[-1]+51.0*(wav[-1]-wav[-2]),wav[-1]-wav[-2])
            wav = scipy.append(wav,x_add_right)
            wav = scipy.append(x_add_left,wav)
            flux = scipy.append(flux,1.0+0.0*x_add_right)
            flux = scipy.append(1.0+0.0*x_add_left,flux)        
        

        tmp = scipy.signal.fftconvolve(flux,y1,'same') * (x1[1]-x1[0])

        if OnePadding:
            tmp = tmp[scipy.where((wav<=wavmax)*(wav>=wavmin))]

        return tmp
        
    elif type(resolution) is str:
        #read in file
        f = open(resolution,'r')
        
        x_profile = []
        y_profile = []
        
        for line in f:
            if '#' in line:
                continue
            
            tmp = line.split()
            #print tmp
            x_profile.append(float(tmp[0]))
            y_profile.append(float(tmp[1]))

        x_profile = scipy.array(x_profile)
        y_profile = scipy.array(y_profile)
        
        #check that arrays are symmetric
        if x_profile[-1] != - x_profile[0]:
            print 'Error in input instrument response function. x-interval has to be symmetric.'
            sys.exit()
        
        if x_profile[ scipy.argmax(y_profile) ] != 0:
            print 'Error in input instrument response function. x-interval must have maximum at x=0.'
            sys.exit()            

        
        cdeltx = wav[1]-wav[0]
        x1 = scipy.arange(x_profile[0],x_profile[-1],cdeltx)
        
        spline = scipy.interpolate.interp1d(x_profile,y_profile)
        y1 = spline(x1)
        
        OnePadding = False #untested for txt-file..
        if OnePadding:#avoid drop in model spectrum near endpoints...
            x_add_left = scipy.arange(wav[0]-50.0*(wav[1]-wav[0]),wav[0],wav[1]-wav[0])
            x_add_right = scipy.arange(wav[-1]+(wav[-1]-wav[-2]),wav[-1]+51.0*(wav[-1]-wav[-2]),wav[-1]-wav[-2])
            wav = scipy.append(wav,x_add_right)
            wav = scipy.append(x_add_left,wav)
            flux = scipy.append(flux,1.0+0*x_add_right)
            flux = scipy.append(1.0+0*x_add_left,flux)   

    #    Time = time.time()
    #    print 'Doing FFT-convolution'
        tmp = scipy.signal.fftconvolve(flux,y1,'same') * (x1[1]-x1[0])

        #    print 'Finished in', time.time()-Time, 'seconds'
        if OnePadding:
            tmp = tmp[scipy.where((wav<=wavmax)*(wav>=wavmin))]
        return tmp
    else:
        print "something wrong with input to ResolutionConvolution"
        

def H(a,x):
    ""
    P  = x**2
    H0 = scipy.exp(-x**2)
    Q  = 1.5/(x**2)
    return H0 - a / scipy.sqrt(pi) / P * ( H0 * H0 * (4. * P * P + 7. * P + 4. + Q) - Q - 1.0 )
   
def DLAflux(Lambda, Lambda0, N, b,f,gam,z):
    "N is column density im 1/cm**2."
    c  = 2.998e10  #;cm/s
    m_e = 9.1095e-28# ;g
    e  = 4.8032e-10# ;cgs units

    C_a  = scipy.sqrt(pi) * e**2 * f * Lambda0 * 1.e-8 / m_e / c / b
    a = Lambda0 * 1.e-8 * gam / (4.*pi*b)
    
    dl_D = b/c*Lambda0
    x = (Lambda/(z+1.0)-Lambda0)/dl_D

    
    #A fix for a numerical instability occuring in H(a,x) when x.min() gets too small.
    #fabs1 = numpy.fabs(x).min()    
    if numpy.fabs(x).min() < 1.0e-5:
        Argmin = numpy.fabs(x).argmin()
        if x[Argmin]>0.0:
            x+= 0.0001218026797#this number is arbitrary. chosen to be prop to 1/prime-number
        else:
            x-=0.0001218026797
        #print 'fabs2', numpy.fabs(x).min(),'old:',fabs1

    
    tau  = C_a*N*H(a,x)


    if numpy.where(scipy.exp(-tau) > 1.0001)[0].shape[0]>0:
        print 'ASD',numpy.min(x**2),fabs1

    
    return scipy.exp(-tau)

#classes
class Parameter:
    "The parameter class in which N,b or z is stored."
    def __init__(self,Value):
        "Set Value and Sigma."
        self.Value = Value
        self.Free = True
    
    def __call__(self):
        "Returns value."
        return self.Value
        
    def Set(self,value):
        "float as input value."
        self.Value = value
    
    def Fix(self):
        "Mark if parameter is not free."
        self.Free = False

        
class AbsorptionLine:
    "An absorption line"
    def __init__(self,ParentComponent=None,Name=None,l0=None,gam=None,f=None):

        self.Name = Name
        self.ParentComponent = ParentComponent
        
        Dict = Pickle_functions.LoadRestartFile('LineDict/Dict.pickle')
        self.l0 = Parameter(Dict[Name][0])
        self.f = Parameter(Dict[Name][1])
        self.gam = Parameter(Dict[Name][2])
        
    def __call__(self,Wavelength):
       "Returns the flux."
       return DLAflux(Wavelength,self.l0(),10.0**self.ParentComponent.logN(),self.ParentComponent.b(),self.f(),self.gam(),self.ParentComponent.z())
        

class Component:
    "A component"
    def __init__(self,Name,logN,b,z,ListOfLines):
        self.Name = Name
        self.ListOfLines = ListOfLines
        
        self.logN = logN
        self.b = b
        self.z = z
        
        #create the absorption lines
        self.DictLines = {}
        for line in self.ListOfLines:
            self.DictLines[line] = AbsorptionLine(Name=line,ParentComponent = self)
    
    def __call__(self,Wavelength):
        "Returns the flux."
        Flux = 1.0
        for line in self.DictLines.keys():
            Flux *= self.DictLines[line](Wavelength)
        
        return Flux
        
    def Set_logN(self,value):
        "float as input value"
        self.logN.Set(value)
    
    def Get_logN(self):
        "Get N"
        return self.logN
            
    def Set_b(self,value):
        "float as input value"
        self.b.Set(value)
    
    def Get_b(self):
        "Get b"
        return self.b            

    def Set_z(self,value):
        "float as input value"
        self.z.Set(value)
    
    def Get_z(self):
        "Get z"
        return self.z        
            
class FitManager:
    "All components / absorption lines"
    def __init__(self):
        "Initializer."
        self.IonDict = {}
        self.DataChunks = {}
        self.LogLikelihoodList = []
        self.Accepted = []
        self.NCallsForCalcLikelihood = 0 # number of times calclikelihood has been called
        
    def __call__(self,Wavelength):
        "Compute flux"
        return self.ComputeFlux(Wavelength)
        
    def ComputeFlux(self,Wavelength):
        "Compute flux"
        Flux = 1.0
        for ion in self.IonDict.keys():
            Flux *= self.IonDict[ion](Wavelength)
            ids = numpy.where(Flux >1.00001)
            if ids[0].shape[0]>0:
                print 'sdfdsf'
                print ion
        return Flux
    
    def GetIonGroup(self,Name=None):
        "Returns dictionairy if Name=None, and the component with key, Name, if Name!=None."
        if Name == None:
            return self.IonDict
        else:
            if Name in self.IonDict.keys():
                return self.IonDict[Name]
            else:
                print 'The component '+ Name + ' is not in key-list '+self.IonDict.keys()
        
    def CreateIonGroup(self, IonName , ListOfLines):
        "Create an ion group"
        if IonName in self.IonDict:
            print '- Warning: Overwriting old ion with name '+IonName
        
        self.IonDict[IonName] = IonGroup(IonName , ListOfLines)
        
        return self.IonDict[IonName]
    
    def PlotChunks(self):
        "This function creates a plot of all the data chunks."
        
        Wavelengths = []
        Keys = []
        for chunk_key in self.DataChunks.keys():
            Wavelengths.append( ( self.DataChunks[chunk_key].LambdaMin + self.DataChunks[chunk_key].LambdaMax ) / 2.0 )
            Keys.append( chunk_key )
        
        Wavelengths = scipy.array(Wavelengths)
        Args = scipy.argsort(Wavelengths)
        Keys = scipy.array(Keys)
        Keys = Keys[Args]
        
        Nchunks = len(self.DataChunks)

        if Nchunks == 1:
            Nx = 1
            Ny = 1
            CreateXLabel = [True]
            CreateYLabel = [True]        
        elif Nchunks in [2,3]:
            Nx = 1
            Ny = 3
            CreateXLabel = [True,True,True]
            CreateYLabel = [True,False,False]
        elif Nchunks == 4:
            Nx = 2
            Ny = 2
            CreateXLabel = [False,False,True,True]
            CreateYLabel = [True,False]*2
        elif Nchunks <= 6:
            Nx = 2
            Ny = 3
            CreateXLabel = [False,False,False,True,True,True]
            CreateYLabel = [True,False,False]*2
        elif Nchunks <= 9:
            Nx = 3
            Ny = 3
            CreateXLabel = [False,False,False]*2+[True,True,True]
            CreateYLabel = [True,False,False]*3
        elif Nchunks <= 12:
            Nx = 3
            Ny = 4            
            CreateXLabel = [False]*8+[True]*4
            CreateYLabel = [True,False,False,False]*3            
        elif Nchunks <= 16:
            Nx = 4
            Ny = 4
            CreateXLabel = [False]*12+[True]*4
            CreateYLabel = [True,False,False,False]*4

        else:
            Nx = 10
            Ny = 10
            CreateXLabel = [False]*100
            CreateYLabel = [True,False,False,False]*100
             
        
        plt.subplots_adjust( right=0.97, top=0.97,wspace=0.05)
        for i in range(len(Keys)):
            key = Keys[i]
            ThisChunk = self.DataChunks[key]
            plt.subplot(Nx,Ny,i+1)
            
            if CreateXLabel[i] == True:
                plt.xlabel(r'$\lambda$ $[\AA]$')
            if CreateYLabel[i] == True:
                plt.ylabel('Flux')
            
            SetLabels(xsize=12,ysize=12*CreateYLabel[i]+1)
            
            plt.ylim((-0.5,2.0))
            plt.xlim((ThisChunk.LambdaMin,ThisChunk.LambdaMax))
            plt.plot(ThisChunk.Wavelength,ThisChunk.Flux,'-',color='black')
            
            x,y=self.GetModelForChunk(ThisChunk)
            plt.plot(x,y,'-',color='blue',lw=2)
        
        return Nx,Ny

        
    def Convolve(self,InstrumentProfile,Wavelength):
        """
        Returns a flux array convolved the instrument resolution. 
        InstrumentProfile can be a string with the filename where the profile is given        
        or InstrumentProfile can be the sigma for a gaussian (as a float). Note, that sigma has to be given as input (and not FWHM).
        
        """
        return ResolutionConvolution(InstrumentProfile,Wavelength,self(Wavelength))

        
    def LinkParameters(self,ComponentList,IonGroups,LinkVariables):
        """
        This function links variables. The components with names given in the ComponentList is linked for the ion groups in the IonGroup-list.

        Example: Fit.LinkParameters(['A','B'],['FeII','CIV'],'zb')
        
        The LinkVariables string:
        
        If 'z' is in string redshifts will be linked.
        
        If 'b' is in string doppler paramers will be linked.
        
        if 'N' is in string column densities will be linked.
        
        """

        if len(ComponentList) == 0 or len(IonGroups) == 0:
            print '-Warning, something is wrong in linking', ComponentList,IonGroups

        #check that all components exist...
        for component in ComponentList:
            for ion in IonGroups:
                if component not in self.IonDict[ion].GetComponent().keys():
                    print 'Component does not exist', component,ion
        
        for component in ComponentList:
            #get z from the first component
            z = self.IonDict[IonGroups[0]].GetComponent(component).Get_z()
            logN = self.IonDict[IonGroups[0]].GetComponent(component).Get_logN()            
            b = self.IonDict[IonGroups[0]].GetComponent(component).Get_b()            
            for ion in IonGroups:
                if 'z' in LinkVariables:
                    self.IonDict[ion].GetComponent(component).z = z

                if 'b' in LinkVariables:
                    self.IonDict[ion].GetComponent(component).b = b                    

                if 'N' in LinkVariables:
                    self.IonDict[ion].GetComponent(component).logN = logN
    
    def GetFreeParameters(self):
        "Get the free parameters. Three lists are returned with b-parameters, N-parameters and z-parameters are returned."
        FreeParameters_logN = []
        FreeParameters_b = []
        FreeParameters_z = []

        for IonKey in sorted(self.IonDict.keys()):
            Ion = self.IonDict[IonKey]

            for component in sorted(Ion.GetComponent().keys()):
                ThisComponent = Ion.GetComponent(component)
                
                if ThisComponent.logN not in FreeParameters_logN:
                    FreeParameters_logN.append(ThisComponent.logN)

                if ThisComponent.b not in FreeParameters_b:
                    FreeParameters_b.append(ThisComponent.b)
                
                if ThisComponent.z not in FreeParameters_z:
                    
                    if ThisComponent.z.Free == True:
                        FreeParameters_z.append(ThisComponent.z)                
        
        return FreeParameters_b, FreeParameters_logN, FreeParameters_z

                
        
        
    def SetFreeParameters(self,ValueArray):
        "Get the free parameters. Three lists are returned with b-parameters, N-parameters and z-parameters are returned."
        FreeParameters_logN = []
        FreeParameters_b = []
        FreeParameters_z = []

        for IonKey in sorted(self.IonDict.keys()):
            Ion = self.IonDict[IonKey]

            for component in sorted(Ion.GetComponent().keys()):
                ThisComponent = Ion.GetComponent(component)
                
                if ThisComponent.logN not in FreeParameters_logN:
                    FreeParameters_logN.append(ThisComponent.logN)

                if ThisComponent.b not in FreeParameters_b:
                    FreeParameters_b.append(ThisComponent.b)
                
                if ThisComponent.z not in FreeParameters_z:
                    if ThisComponent.z.Free == True:                    
                        FreeParameters_z.append(ThisComponent.z)                
        
        
        ParamNo = 0
        
        for i in range(len(FreeParameters_b)):
            FreeParameters_b[i].Set(ValueArray[ParamNo])
            ParamNo += 1
            
        for i in range(len(FreeParameters_logN)):
            FreeParameters_logN[i].Set(ValueArray[ParamNo]) 
            ParamNo += 1            
            
        for i in range(len(FreeParameters_z)):
            FreeParameters_z[i].Set(ValueArray[ParamNo])             
            ParamNo += 1            

        if ParamNo != len(ValueArray):
            print 'Something terribly wrong,voivdmsdmsvkl'
            sys.exit()        
        
            
    def GetlogN(self,Ion,Component):
        "Get logN for a given component of an iongroup"
        return self.IonDict[Ion].Components[Component].logN

    def Getb(self,Ion,Component):
        "Get b for a given component of an iongroup"
        return self.IonDict[Ion].Components[Component].b
        
    def Getz(self,Ion,Component):
        "Get z for a given component of an iongroup"
        return self.IonDict[Ion].Components[Component].z
        

    def CreateFreeParamDiagram(self,FilePrefix):
        "Create a diagram with the iongroups, components and free parameters. Make sure graphviz is installed."
        FreeParameters_logN = []
        FreeParameters_b = []
        FreeParameters_z = []
        for IonKey in self.IonDict.keys():
            Ion = self.IonDict[IonKey]
            for component in Ion.GetComponent().keys():
                ThisComponent = Ion.GetComponent(component)
                
                if ThisComponent.logN not in FreeParameters_logN:
                    FreeParameters_logN.append(ThisComponent.logN)

                if ThisComponent.b not in FreeParameters_b:
                    FreeParameters_b.append(ThisComponent.b)
                
                if ThisComponent.z not in FreeParameters_z:
                    FreeParameters_z.append(ThisComponent.z)
        
            
        File = open(FilePrefix+'.dot','w+')
        File.write('digraph fit {\n')
        File.write('node [ fontname=Palatino,  fontsize=100];\nratio=0.5;\n')
        
        ion_group_keys = self.IonDict.keys()
        for i in range(len(ion_group_keys)):            
            components = self.IonDict[ion_group_keys[i]]
            components_keys = components.GetComponent().keys()
            for j in range(len(components_keys)):
                key = components_keys[j]
                File.write( ion_group_keys[i]+' -> '+key+'_'+str(i)+';' )
                File.write( key+'_'+str(i)+' -> '+'"logN_'+str(id(components.GetComponent(key).logN))+', %G'%components.GetComponent(key).logN()+'";\n' )
                File.write( key+'_'+str(i)+' -> '+'"z_'+str(id(components.GetComponent(key).z))+', %f'%components.GetComponent(key).z()+'";\n' )
                File.write( key+'_'+str(i)+' -> '+'"b_'+str(id(components.GetComponent(key).b))+', %f'%components.GetComponent(key).b()+'";\n' )                
        File.write('}\n')
        File.close()
        
        commands.getoutput('dot ' + FilePrefix+'.dot' + ' -Tps -o '+str(FilePrefix)+'.ps')
            

    def AddDataChunk(self,Filename, DictKey,LambdaMin, LambdaMax,Resolution):
        "Add data chunk."
        self.DataChunks[DictKey] =   DataChunk(Filename, LambdaMin, LambdaMax,Resolution)
    
    def GetDataChunk(self,key=None):
        "Get data chunk."
        if key not in self.DataChunks.keys():
            return self.DataChunks
        else:
            return self.DataChunks[key]
    
    def CalcLnLikelihood(self):
        "returns -ln(Likelihood)"
        lnL = 0.0
        
        for chunk_key in self.DataChunks.keys():
            chunk = self.DataChunks[chunk_key]

            Wavelength = scipy.arange(chunk.GetLambdaMin()-10.0,10.0+chunk.GetLambdaMax(),0.005)
            
            Flux_convolve =  self.Convolve(chunk.Resolution,Wavelength)
            
            spline = scipy.interpolate.interp1d(Wavelength,Flux_convolve)
            Flux_obs = spline(chunk.GetWavelength())
            
            ids = numpy.where(Flux_obs>1.0001)
            
            if ids[0].shape[0]>0:
                print 'John John, look here!'
                self.PlotChunks()
                plt.show()
            
            lnL += scipy.sum(   ( chunk.GetFlux() - Flux_obs )**2 /  2.0 / chunk.GetError()**2  + scipy.log(scipy.sqrt( 2.0 * pi ) * chunk.GetError() )  )
        
        
        #if self.NCallsForCalcLikelihood%50 == 0:
        #    self.PlotChunks()
        #    print self.NCallsForCalcLikelihood
            #plt.show()
        #    self.CreateFreeParamDiagram(FilePrefix=str(self.NCallsForCalcLikelihood))
        
        self.NCallsForCalcLikelihood += 1
        
        
        return lnL

        
    def GetModelForChunk(self,Chunk):
        "Get lambda,model_flux for a data chunk"
        Wavelength = scipy.arange(Chunk.GetLambdaMin()-10.0,10.0+Chunk.GetLambdaMax(),0.005)
        Flux_convolve =  self.Convolve(Chunk.Resolution,Wavelength)
        return Wavelength,Flux_convolve
            
#    def GetModelForWavelengths(self,Wavelengths,Resolution=0.001):
#        "Wavelengths has to be an numpy-array in angstrom. Normalized flux is returned"
#        Wavelength = scipy.arange(Wavelengths[0]-10.0,10.0+Wavelengths[-1],0.005)
#        Flux_convolve =  self.Convolve(Resolution,Wavelength)
#        return Wavelength,Flux_convolve
        

    def GetParameterArray(self):
        b,logN,z = self.GetFreeParameters()#make sure that they are sorted
        
                
        ndim = len(b)+len(logN)+len(z)
        
        ParamValues = []
        ParamTypes = []
        for i in range(len(b)):
            ParamValues.append(b[i]())
            ParamTypes.append('b')

        for i in range(len(logN)):
            ParamValues.append(logN[i]())
            ParamTypes.append('logN')            
            
        for i in range(len(z)):
            ParamValues.append(z[i]())
            ParamTypes.append('z')            
            
        ParamValues = numpy.array(ParamValues)
        
        return ParamValues,ParamTypes
    
 
    def CreateWalkerGuesses(self,DeltaB,DeltaLogN,DeltaZ,ndim,nwalkers):
        """
        DeltaB: b-values are distributed in the interval uniform(1.0-DeltaB,1.0+DeltaB) * <b>
        DeltaLogN: LogN-values are distributed in the interval '<logN> +/- uniform(-DeltaLogN,+DeltaLogN)
        DeltaZ: z-values are distributed in the interval uniform(1.0-DeltaZ,1.0+DeltaZ)
        """
        ParamArray,TypeArray = self.GetParameterArray()
        if ndim != len(ParamArray):
            print 'something terribly wrong,njisbv'
            sys.exit()

        A = []
        for i in range(nwalkers):
            tmp = numpy.zeros(len(ParamArray))
            
            for j in range(len(tmp)):
                if TypeArray[j] == 'b':
                    tmp[j] = random.uniform(1.0-DeltaB,1.0+DeltaB) * ParamArray[j]

                if TypeArray[j] == 'logN':
                    tmp[j] = random.uniform(-DeltaLogN,DeltaLogN) + ParamArray[j]           

                if TypeArray[j] == 'z':
                    tmp[j] = random.uniform(1.0-DeltaZ,1.0+DeltaZ) * ParamArray[j]

            A.append( tmp )        
        
        return A

class IonGroup:
    "An Ion Group."
    def __init__(self, IonName , ListOfLines):
        "Initializer."
        self.IonName = IonName
        self.ListOfLines = ListOfLines
        self.Components = {}

    def __call__(self,Wavelength):
        "Returns the flux."
        Flux = 1.0
        for component in self.Components.keys():
            Flux *= self.Components[component](Wavelength)
                
        return Flux
    
    def AddComponent(self, ComponentName, logN,b,z):
        "Add a component"
        logN = Parameter(logN)
        b = Parameter(b)
        z = Parameter(z)

        self.Components[ComponentName] = Component(ComponentName,logN,b,z,self.ListOfLines)

    def GetComponent(self,Name=None):
        "Returns dictionairy if Name=None, and the component with key, Name, if Name!=None."
        if Name == None:
            return self.Components
        else:
            if Name in self.Components.keys():
                return self.Components[Name]
            else:
                print 'The component '+ Name + ' is not in key-list '+self.Components.keys()


class DataChunk:
    def __init__(self,Filename, LambdaMin, LambdaMax,Resolution):
        "Lambda is in angstrom. resolution is in angstrom (sigma, and not FWHM)"
        self.Filename = Filename
        self.LambdaMin = LambdaMin
        self.LambdaMax = LambdaMax
        self.Resolution = Resolution

        f = open(Filename,'r')

        Wavelength = []
        Flux = []
        Error = []

        for line in f:
            if '#' in line:
                continue
            tmp = line.split()

            Wavelength.append(float(tmp[0]))
            Flux.append(float(tmp[1]))
            Error.append(float(tmp[2]))

        f.close()

        Wavelength = scipy.array(Wavelength)
        Flux = scipy.array(Flux)
        Error = scipy.array(Error)

        GoodIDs = scipy.where( ( Wavelength > LambdaMin ) * ( Wavelength < LambdaMax ) )

        self.Wavelength = Wavelength[GoodIDs]
        self.Flux = Flux[GoodIDs]
        self.Error = Error[GoodIDs]
    
    def GetWavelength(self):
        return self.Wavelength

    def GetFlux(self):
        return self.Flux
        
    def GetError(self):
        return self.Error
        
    def GetLambdaMin(self):
        return self.LambdaMin        
        
    def GetLambdaMax(self):
        return self.LambdaMax    