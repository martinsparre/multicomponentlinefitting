import scipy
from ROOT import TH2D
import matplotlib.pyplot as plt
import scipy.integrate
from math import pi
import VoigtFitting.VoigtFitting as VoigtFitting


def GetLevels(DataArray):
    TotalVolume = scipy.sum(DataArray)

    Guess=0.99*DataArray.max()
    ThisVolume = scipy.sum(DataArray[scipy.where(DataArray>Guess)])
    Fraction = ThisVolume/TotalVolume
    while Fraction < 0.67 or Fraction > 0.69:
        
        if Fraction < 0.67:
            Guess /=1.0005
        if Fraction > 0.69:
            Guess *=1.0005

        ThisVolume = scipy.sum(DataArray[scipy.where(DataArray>Guess)])
        Fraction = ThisVolume/TotalVolume
        

      #  print Guess,Fraction

    Level1=Guess

    Guess=0.99*DataArray.max()
    ThisVolume = scipy.sum(DataArray[scipy.where(DataArray>Guess)])
    Fraction = ThisVolume/TotalVolume
    while Fraction < 0.94 or Fraction > 0.96:
        
        if Fraction < 0.94:
            Guess /=1.0005
        if Fraction > 0.96:
            Guess *=1.0005

        ThisVolume = scipy.sum(DataArray[scipy.where(DataArray>Guess)])
        Fraction = ThisVolume/TotalVolume
        

       # print Guess,Fraction

    Level2=Guess


    Guess=0.99*DataArray.max()
    ThisVolume = scipy.sum(DataArray[scipy.where(DataArray>Guess)])
    Fraction = ThisVolume/TotalVolume
    while Fraction < 0.98 or Fraction > 0.99:
        
        if Fraction < 0.98:
            Guess /=1.0005
        if Fraction > 0.99:
            Guess *=1.0005

        ThisVolume = scipy.sum(DataArray[scipy.where(DataArray>Guess)])
        Fraction = ThisVolume/TotalVolume
        

    Level3=Guess
    
    return Level1,Level2,Level3

    
def PlotContours(x,y,Color='blue'):
    Hist = TH2D("Hist1","Hist1",40,x.min()-2,x.max()+2,40,y.min()-0.3,y.max()+0.3)

    for i in range(len(x)):
        Hist.Fill(x[i],y[i])

    Hist.Sumw2()
    Hist.Smooth(2,'k3a')

    x1 = scipy.linspace(x.min()-2,x.max()+2,60)
    y1 = scipy.linspace(y.min()-0.3,y.max()+0.3,60)

    x2 = []
    y2 = []
    z2 = scipy.zeros((len(x1),len(y1)))
    for i in range(len(x1)):
        for j in range(len(y1)):
            x2.append(x1[i])
            y2.append(y1[j])
            z2[j,i] = Hist.Interpolate(x1[i],y1[j])

    Level1,Level2,Level3 = GetLevels(z2)

    plt.contour(x1,y1,z2,levels=[Level1,Level2,Level3],linestyles = ['solid','dotted','dashed'],colors=[Color]*3,linewidths=2)    
    

def PlotEW(LineName ,Redshift,EWmin = 0.0, EWmax=4.0, DeltaEW=0.5):

    Line = VoigtFitting.AbsorptionLine(Name=LineName)
    
    NGrid = 100
    b = scipy.linspace(1e5,45e5,NGrid)
    N = scipy.linspace(13.0,20.0,NGrid)

    EW = scipy.zeros((NGrid,NGrid))

    EqWidth = []
    for i in range(NGrid):
        for j in range(NGrid):
            EW[j,i] = GetEW(b[i],10**N[j],Redshift,Line.l0(),Line.f(),Line.gam())

    levels=scipy.arange(EWmin,EWmax,DeltaEW)
    CS = plt.contourf(b/1.0e5,N,EW, levels=levels,cmap = plt.cm.get_cmap("gray"))
    cbar = plt.colorbar(CS)
    cbar.ax.set_ylabel('EW (Angstrom)', fontsize=14)
    
def GetEW(b ,N ,z,Lambda0,f,gam):
    Lambda0Redshifted = (z+1.0) * Lambda0
    Lambda = scipy.linspace(Lambda0Redshifted-10.0,Lambda0Redshifted+10.0,200)
    Flux = DLAflux(Lambda, Lambda0, N, b,f,gam,z)

    EW = 20.0 - scipy.integrate.trapz(Flux, x=Lambda)
    return EW

    
    
def Gauss(x,s,mu=0):
    return scipy.exp(-(x-mu)**2.0/(2*s**2))

def H(a,x):
    ""
    P  = x**2
    H0 = scipy.exp(-x**2)
    Q  = 1.5/x**2
    return H0 - a / scipy.sqrt(pi) / P * ( H0 * H0 * (4. * P * P + 7. * P + 4. + Q) - Q - 1.0 )
   
def DLAflux(Lambda, Lambda0, N, b,f,gam,z):
    c  = 2.998e10  #;cm/s
    m_e = 9.1095e-28# ;g
    e  = 4.8032e-10# ;cgs units

    C_a  = scipy.sqrt(pi) * e**2 * f * Lambda0 * 1.e-8 / m_e / c / b
    a = Lambda0 * 1.e-8 * gam / (4.*pi*b)
    
    dl_D = b/c*Lambda0
    x = (Lambda/(z+1.0)-Lambda0)/dl_D+0.01
    
    tau  = C_a*N*H(a,x)
    return scipy.exp(-tau)    