import matplotlib.pyplot as plt
import scipy
from AbsorptionLine.AbsorptionLine import AbsorptionLine, ResolutionConvolution

#Array with VIS-wavelength
Wavelength = scipy.arange(5000.0,10000.0,0.005)

#define emission lines:
#CrIV = Ion('CIV',['CIV_1548','CIV_1550'])
#CrIV.AddComponent(11.0e14, 4000000,4.98)
#CrIV.AddComponent(15e14,2000000,4.9814)


CrIVA = AbsorptionLine(11.0e14, 4000000,4.98,Name='CIV_1548')
CrIVB = AbsorptionLine(15e14,2000000,4.9814,Name='CIV_1548')

#Calculate flux
flux = CrIVA(Wavelength) * CrIVB(Wavelength)

#Instrument resolution:
flux_convolve = ResolutionConvolution(1.25/2.35482,Wavelength,flux)



sys.exit()

flux_convolve1 = ResolutionConvolution(1.25/2.35/2,Wavelength,flux)



#Put on another grid:
Wavelength_obs = scipy.arange(5000.0,10000.0,0.25)
spline = scipy.interpolate.interp1d(Wavelength,flux_convolve)
Flux_obs = spline(Wavelength_obs)

#Add gaussian noise
flux_noise = flux_convolve + scipy.random.normal(0,0.1,len(Wavelength))

#the plot
z = 4.98

plt.plot(Wavelength,flux_noise,'-',color='blue',label='With noise',lw=1)
plt.plot(Wavelength,flux,'-',color='black',label='Not convolved',lw=2)
plt.plot(Wavelength,flux_convolve,'--',color='black',label='Convolved with resolution',lw=2)
plt.plot(Wavelength,flux_convolve1,'--',color='red',label='Better resolution',lw=2)
plt.plot(Wavelength,CrIVA(Wavelength),'-',color='green',lw=2)
plt.plot(Wavelength,CrIVB(Wavelength),'-',color='maroon',lw=2)
plt.xlim(((z+1)*CrIVA.l0()-20,(z+1)*CrIVA.l0()+20))
plt.ylim((-1,2))
plt.legend(loc=4)
plt.grid()
plt.xlabel('Wavelength [Angstrom]',fontsize=16)
plt.ylabel('Normalized flux',fontsize=16)
plt.show()