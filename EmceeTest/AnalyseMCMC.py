import matplotlib.pyplot as plt
import scipy,numpy,random
import VoigtFitting.VoigtFitting as VoigtFitting
import LineDict.Pickle_functions as Pickle_functions
import Contours
import sys
import pylab
#import AbsorptionLine, ResolutionConvolution,FitManager

plt.rc('font',**{'family':'sans-serif','sans-serif':['Computer Modern Roman']})
plt.rc('font',**{'family':'serif','serif':['Computer Modern Sans serif']})
plt.rc('text', usetex=True)

plt.rcParams["xtick.major.size"] = 5
plt.rcParams["ytick.major.size"] = 5

#Functions:
def SetLabels(xsize=16,ysize=16):
    ax = pylab.gca()
    for tick in ax.xaxis.get_major_ticks():
        tick.label1.set_fontsize(xsize)

    for tick in ax.yaxis.get_major_ticks():
        tick.label1.set_fontsize(ysize)
        
        



tmp = Pickle_functions.LoadRestartFile('MCMC_Output.pickle')

pos = tmp[0]
prob = tmp[1]
state = tmp[2]
flatchain = tmp[3]

################################
#### Do some plotting       ####
################################

plt.subplots_adjust(left=0.1, bottom=0.13, right=0.97, top=0.91,wspace=0.31, hspace=0.35)

plt.subplot(1,2,1)
b=flatchain[:,0]
N=flatchain[:,4]

#plt.plot(b/1.0e5,N,'.',color='blue',ms=0.4)
Contours.PlotEW('SII_1253',5.0)
Contours.PlotContours(b/1.0e5,N)

b=flatchain[:,1]
N=flatchain[:,5]
#plt.plot(b/1.0e5,N,'.',color='blue',ms=0.2)
Contours.PlotContours(b/1.0e5,N,Color='red')
plt.title('SII 1253 fit',fontsize=20)
plt.xlabel('$b$ (km s$^{-1}$)',fontsize=20)
plt.ylabel('$\log N / $cm$^{-2}$',fontsize=20)
SetLabels()
plt.ylim((14.5,20))
plt.xlim((5.0,45.0))

plt.plot([-10,-9],[-10,-9],'-',lw=2,label='$z=4.991$',color='red')
plt.plot([-10,-9],[-10,-9],'-',lw=2,label='$z=4.990$',color='blue')
plt.legend(prop=dict(size=16), numpoints=1, ncol=1,frameon=True,loc=1)


plt.subplot(1,2,2)
b=flatchain[:,0]
N=flatchain[:,2]
#plt.plot(b/1.0e5,N,'.',color='blue',ms=0.4)
Contours.PlotEW('NiII_1370',5.0)
Contours.PlotContours(b/1.0e5,N)

b = flatchain[:,1]
N = flatchain[:,3]
#plt.plot(b/1.0e5,N,'.',color='blue',ms=0.2)
Contours.PlotContours(b/1.0e5,N,Color='red')

plt.title('NiII 1370 fit',fontsize=20   )
plt.xlabel('$b$ (km s$^{-1}$)',fontsize=20)
plt.ylabel('$\log N / $cm$^{-2}$',fontsize=20)
plt.ylim((13.7,17))
plt.xlim((5.0,45.0))
plt.plot([-10,-9],[-10,-9],'-',lw=2,label='$z=4.991$',color='red')
plt.plot([-10,-9],[-10,-9],'-',lw=2,label='$z=4.990$',color='blue')
SetLabels()
plt.legend(prop=dict(size=16), numpoints=1, ncol=1,frameon=True,loc=1)
plt.show()

plt.subplot(2,2,3)
plt.hist(numpy.log10(10.0**flatchain[:,4]),bins=40,color='orange',normed=True)
plt.hist(numpy.log10(10.0**flatchain[:,5]),bins=40,color='green',normed=True)
plt.title('SII probability distribution',fontsize=22)
plt.xlabel('$\log N / $cm$^{-2}$',fontsize=20)
plt.ylabel('Histogram',fontsize=20)

print numpy.mean(numpy.log10(10.0**flatchain[:,4])),'+/-',numpy.std(numpy.log10(10.0**flatchain[:,4]))

raw_input()
plt.subplot(2,2,4)
plt.hist(numpy.log10(10.0**flatchain[:,2]),bins=40,color='orange',normed=True)
plt.hist(numpy.log10(10.0**flatchain[:,3]),bins=40,color='green',normed=True)
plt.title('NiII probability distribution',fontsize=22)
plt.xlabel('$\log N / $cm$^{-2}$',fontsize=20)
plt.ylabel('Histogram',fontsize=20)


plt.show()

plt.subplot(2,3,5)
N = flatchain[:,2]
plt.plot(range(len(N)),N,'.',ms=0.5)
print 'N',len(N)
raw_input()
N = flatchain[:,3]
plt.plot(range(len(N)),N,'.',ms=0.5)

N = flatchain[:,4]
plt.plot(range(len(N)),N,'.',ms=0.5)

N = flatchain[:,5]
plt.plot(range(len(N)),N,'.',ms=0.5)


plt.subplot(2,3,6)
b = flatchain[:,0]
plt.plot(range(len(b)),b/1.0e5,'.',ms=0.5,color='green')

b=flatchain[:,1]
plt.plot(range(len(b)),b/1.0e5,'.',ms=0.5,color='orange')



plt.subplot(2,3,1)
z=flatchain[:,6]
plt.plot(range(len(z)),z,'.',ms=0.5,color='green')

z=flatchain[:,7]
plt.plot(range(len(z)),z,'.',ms=0.5,color='orange')

plt.show()



plt.show()