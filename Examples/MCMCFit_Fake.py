import matplotlib.pyplot as plt
import scipy
import VoigtFitting.VoigtFitting as VoigtFitting
import time,copy
#import AbsorptionLine, ResolutionConvolution,FitManager


Fit = VoigtFitting.FitManager()

#read data:
#Fit.AddDataChunk('Data/NormVIS_Angstrom.txt', '9275A', 9250.0, 9297.0,1.25 / 2.35482)
#Fit.AddDataChunk('Data/FirstTest_Fake.txt', '9640A',9620.0, 9660.0,1.25 / 2.35482)

Fit.AddDataChunk('Data/FirstTest_Fake.txt', '9640A',9620.0, 9660.0,1.25 / 2.35482)

#define lines
#Ion = Fit.CreateIonGroup('CIV',['CIV_1548','CIV_1550'])
#Ion.AddComponent('A',11.0e14, 3600000,4.9887)
#Ion.AddComponent('B',1e14,1000000,4.99)

Ion = Fit.CreateIonGroup('FeII',['FeII_1608','FeII_1611'])
Ion.AddComponent('A',3e16, 3000000,4.989)
#Ion.AddComponent('B',6.0e16, 300000,4.99)


#Fit.LinkParameters(['A','B'],['FeII','CIV'],'z')
#Fit.Link(['B'],['FeII','CIV'],'zb')


#print Fit.CalcLnLikelihood()


#Fit.CreateFreeParamDiagram()

Fit1 = copy.deepcopy(Fit)

Fit.RunMCMC(500)
Fit1.RunMCMC(500)


b,N,z = Fit.GetFreeParameters()
b1,N1,z1 = Fit1.GetFreeParameters()
print 'Nacc=',len(b[0].ListOfAcceptedValues)


plt.subplot(2,3,1)
plt.xlabel('Iteration Number')
plt.ylabel('-ln L')

plt.plot(range(len(Fit.LogLikelihoodList)),Fit.LogLikelihoodList,'-',ms=5, lw=0.7,mew=0,color='red')
plt.plot(range(len(Fit1.LogLikelihoodList)),Fit1.LogLikelihoodList,'-',ms=5, lw=0.7,mew=0,color='blue')



plt.subplot(2,3,2)
plt.xlabel('Iteration Number')
plt.ylabel('b')
for i in b:
    plt.plot(range(len(i.ListOfAcceptedValues)),i.ListOfAcceptedValues,'-',ms=5, lw=0.7,mew=0,color='red')

for i in b1:
    plt.plot(range(len(i.ListOfAcceptedValues)),i.ListOfAcceptedValues,'-',ms=5,lw=0.7,mew=0,color='blue')    
    
    
#    print 'b',i()
plt.subplot(2,3,3)
plt.xlabel('Iteration Number')
plt.ylabel('N')
for i in N:
    plt.plot(range(len(i.ListOfAcceptedValues)),i.ListOfAcceptedValues,'-',ms=5, lw=0.7,mew=0,color='red')

for i in N1:
    plt.plot(range(len(i.ListOfAcceptedValues)),i.ListOfAcceptedValues,'-',ms=5, lw=0.7,mew=0,color='blue')
    
plt.subplot(2,3,4)
plt.xlabel('Iteration Number')
plt.ylabel('z')
for i in z:
    plt.plot(range(len(i.ListOfAcceptedValues)),i.ListOfAcceptedValues,'-',ms=5, lw=0.7,mew=0,color='red')

for i in z1:
    plt.plot(range(len(i.ListOfAcceptedValues)),i.ListOfAcceptedValues,'-',ms=5, lw=0.7,mew=0,color='blue')
    
    
plt.subplot(2,3,5)
plt.xlabel('b')
plt.ylabel('N')
plt.plot(b[0].ListOfAcceptedValues,N[0].ListOfAcceptedValues,'-o',ms=3, mew=0,color='red')
plt.plot(b1[0].ListOfAcceptedValues,N1[0].ListOfAcceptedValues,'-o',ms=3, mew=0,color='blue')

#for i in z:
#    print 'z',i()
plt.show()
#import sys
#sys.exit()

Wavelength = scipy.arange(5000.0,10000.0,0.005)
Flux = Fit.ComputeFlux(Wavelength)

#Instrument resolution:
Flux_convolve =  Fit.ConvolveWithGaussian(1.25/2.35482,Wavelength)

#Put on another grid:
Wavelength_obs = scipy.arange(5000.0,10000.0,0.25)
spline = scipy.interpolate.interp1d(Wavelength,Flux_convolve)
Flux_obs = spline(Wavelength_obs)

#Add gaussian noise
Flux_noise = Flux_obs + scipy.random.normal(0,0.1,len(Wavelength_obs))

z = 4.989

#plt.plot(Wavelength_obs,Flux_noise,'-',color='grey',label='With noise',lw=2)
plt.plot(Wavelength,Flux,'-',color='black',label='Not convolved',lw=2)
plt.plot(Wavelength,Flux_convolve,'--',color='black',label='Convolved with resolution',lw=2)
#plt.plot(Fit.GetDataChunk('9275A').GetWavelength(),Fit.GetDataChunk('9275A').GetFlux(),'-',color='blue',label='With noise',lw=1)
plt.plot(Fit.GetDataChunk('9640A').GetWavelength(),Fit.GetDataChunk('9640A').GetFlux(),'-',color='blue',label='Data',lw=1)
plt.xlim((9000,10000))
plt.ylim((-1,2))
plt.legend(loc=4)
plt.grid()
plt.xlabel('Wavelength [Angstrom]',fontsize=16)
plt.ylabel('Normalized flux',fontsize=16)
plt.show()


