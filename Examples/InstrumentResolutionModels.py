import matplotlib.pyplot as plt
import scipy
import VoigtFitting.VoigtFitting as VoigtFitting
import time
#import AbsorptionLine, ResolutionConvolution,FitManager


Fit = VoigtFitting.FitManager()


Ion = Fit.CreateIonGroup('FeII',['FeII_1608','FeII_1611'])
Ion.AddComponent('A',3.0e15, 1500000,4.9887)
Ion.AddComponent('B',1.0e14, 500000,4.9887)

#print Fit.CalcLnLikelihood()

Wavelength = scipy.arange(9000.0,10000.0,0.005)
Flux = Fit.ComputeFlux(Wavelength)

plt.plot(Wavelength,Flux,'-',color='black',label=r'$\delta$-function instrument profile',lw=2)

#Gaussian instrument profile:
Flux_convolve =  Fit.Convolve(1.25/2.35482,Wavelength)

plt.plot(Wavelength,Flux_convolve,'-',color='blue',label='Gaussian intrument profile',lw=2)

#Arbitrary instrument profile:
#Flux_convolve1 =  Fit.Convolve('Data/GaussInstrumentProfile.txt',Wavelength)
#plt.plot(Wavelength,Flux_convolve1,':',color='blue',label='Convolved with gaussian',lw=3)

#Arbitrary instrument profile:
Flux_convolve1 =  Fit.Convolve('Data/Prof.txt',Wavelength)
plt.plot(Wavelength,Flux_convolve1,'--',color='red',label='Measured instrument profile',lw=2)

#Arbitrary instrument profile:
#Flux_convolve1 =  Fit.Convolve('Data/Prof1.txt',Wavelength)
#plt.plot(Wavelength,Flux_convolve1,'--',color='green',label='Measured instrument profile',lw=2)

plt.xlim((9627,9657))
plt.ylim((-0.1,1.2))
plt.legend(loc=4)
plt.grid()
plt.xlabel('Wavelength [Angstrom]',fontsize=16)
plt.ylabel('Normalized flux',fontsize=16)
plt.show()


