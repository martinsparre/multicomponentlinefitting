.. MCMCLineFitting documentation master file, created by
   sphinx-quickstart on Sat Apr 21 14:48:29 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MCMCLineFitting's documentation!
===========================================

Contents:

.. toctree::
   :maxdepth: 2

Classes
==================


.. automodule:: VoigtFitting.VoigtFitting
    :members:
    

Functions
=======================

.. autofunction:: VoigtFitting.VoigtFitting.ResolutionConvolution

    
Example
==================

Here is an example of usage:

.. code-block:: python

    import matplotlib.pyplot as plt
    import scipy
    import VoigtFitting.VoigtFitting as VoigtFitting
    import time


    Fit = VoigtFitting.FitManager()

    #read data:
    Fit.AddDataChunk('Data/NormVIS_Angstrom.txt', '9275A', 9250.0, 9297.0,1.25 / 2.35482)
    Fit.AddDataChunk('Data/NormVIS_Angstrom.txt', '9640A',9620.0, 9660.0,1.25 / 2.35482)

    #define lines
    Ion = Fit.CreateIonGroup('CIV',['CIV_1548','CIV_1550'])
    Ion.AddComponent('A',11.0e14, 3600000,4.9887)
    Ion.AddComponent('B',1e14,1000000,4.99)

    Ion = Fit.CreateIonGroup('FeII',['FeII_1608','FeII_1611'])
    Ion.AddComponent('A',6.0e15, 3000000,4.9887)
    Ion.AddComponent('B',6.0e16, 300000,4.99)
    Ion.AddComponent('C',6.0e16, 300000,4.991)

    Fit.LinkParameters(['A','B'],['FeII','CIV'],'zb')
    #Fit.Link(['B'],['FeII','CIV'],'zb')


    print Fit.CalcLnLikelihood()


    Fit.CreateFreeParamDiagram()
