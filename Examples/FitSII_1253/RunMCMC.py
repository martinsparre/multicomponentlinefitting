import matplotlib.pyplot as plt
import scipy
import VoigtFitting.VoigtFitting as VoigtFitting
import time
import LineDict.Pickle_functions as Pickle_functions
#import AbsorptionLine, ResolutionConvolution,FitManager


Fit = VoigtFitting.FitManager()

#read data:
Fit.AddDataChunk('/home/ms/Uni/Observations/111008/Data/AirToVac/SII_spec.txt', '9640A',7503.0, 7518.0,0.25)

#define lines
#Ion = Fit.CreateIonGroup('CIV',['CIV_1548','CIV_1550'])
#Ion.AddComponent('A',11.0e14, 3600000,4.9887)
#Ion.AddComponent('B',1e14,1000000,4.99)

Ion = Fit.CreateIonGroup('SII',['SII_1253'])
Ion.AddComponent('A',10.0**17.79, 1016000,4.989800)
Ion.AddComponent('B',10**17.1372, 1030000,4.991488)



#Fit.LinkParameters(['A','B'],['FeII','CIV'],'z')
#Fit.Link(['B'],['FeII','CIV'],'zb')


#print Fit.CalcLnLikelihood()


#Fit.CreateFreeParamDiagram()


Fit.RunMCMC(9000)

Pickle_functions.SaveToRestartFile('SII_1253.pickle',Fit)

Fit.RemovePoints(500,5)

b,N,z = Fit.GetFreeParameters()
print 'Nacc=',len(b[0].ListOfAcceptedValues)

#plt.subplot(2,2,1)
#plt.xlabel('Iteration Number')
#plt.ylabel('b')
#for i in b:
    #plt.plot(range(len(i.ListOfAcceptedValues)),i.ListOfAcceptedValues,'-o')

    
##    print 'b',i()
#plt.subplot(2,2,2)
#plt.xlabel('Iteration Number')
#plt.ylabel('N')
#for i in N:
##    print 'N',i()
    #plt.plot(range(len(i.ListOfAcceptedValues)),scipy.log10(i.ListOfAcceptedValues),'-o')

    
#plt.subplot(2,2,3)
#plt.xlabel('Iteration Number')
#plt.ylabel('z')
#for i in z:
##    print 'N',i()
    #plt.plot(range(len(i.ListOfAcceptedValues)),i.ListOfAcceptedValues,'-o')



plt.xlabel(r'$b$')
plt.ylabel(r'$N$')
plt.plot(scipy.array(b[0].ListOfAcceptedValues)/1e5,scipy.log10(N[0].ListOfAcceptedValues),'.',zorder=4)    
plt.errorbar(2900000/1e5,14.9,xerr=600000/1e5   ,yerr=0.33,color='red',zorder=3)

plt.show()
