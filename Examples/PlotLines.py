import matplotlib.pyplot as plt
import scipy
import VoigtFitting.VoigtFitting as VoigtFitting
import time
#import AbsorptionLine, ResolutionConvolution,FitManager


Fit = VoigtFitting.FitManager()

#read data:
#Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '9275A', 9250.0, 9297.0,1.25 / 2.35482)
#Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '9640A',9620.0, 9660.0,1.25 / 2.35482)

Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '7460',7460.0, 7585.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '7790',7790.0, 7870,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '7980',7980.0, 8150.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '8333',8200.0, 8410.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '8540',8400.0, 8710.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '9120',9120,9400,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '9580',9580.0, 9670.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormVIS_Angstrom.txt', '9950',9950.0, 10050.0,1.25 / 2.35482)


Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '10800',10760.0, 10850.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '11060',11060.0, 11130.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '11240',11210.0, 11300.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '13250',13250.0, 13500.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '15470',15470.0, 15590.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '16725',16725.0, 16815.0,1.25 / 2.35482)
Fit.AddDataChunk('Data/111008/NormNIR_Angstrom.txt', '17060',17060.0, 17115.0,1.25 / 2.35482)



    





Arrow = lambda text,posx,col: plt.annotate(text, xy=(  posx, 1.2), xytext=(posx,1.8),fontsize=10,ha='center',arrowprops=dict(facecolor=col, shrink=0.2))
Nx,Ny = Fit.PlotChunks()
plt.subplot(Nx,Ny,1)
Arrow('SII1250', 7486.6,'blue')
Arrow('SII1253',7508.5,'blue')
Arrow('SII1259', 7546.6,'blue')
Arrow('SiII*1264?', 7575.1,'blue')

plt.subplot(Nx,Ny,2)
Arrow('OI1302', 7798.6 ,'blue')
Arrow('SiII1304', 7813.7,'blue' )
Arrow('OI*1305',7820.0 ,'blue')
Arrow('SiII*1309', 7842.5,'blue')
Arrow('SiIV', 7864.5 , 'red')

plt.subplot(Nx,Ny,3)
Arrow('CII1334', 7994.5,'blue')
Arrow('CII*1335',7999.0  ,'blue')
Arrow('OI1355?', 8125.0 ,'blue')

plt.subplot(Nx,Ny,4)
Arrow('NiII1370?', 8206.4,'blue')
Arrow('SiIV1393 ', 8347.1,'blue')
Arrow('SiIV1402', 8402.0,'blue')
plt.subplot(Nx,Ny,5)
Arrow('NiII1502?', 8440.0,'red')
Arrow('SiII1526 ',8562.6 ,'red')
Arrow('CIV1548', 8679.9 ,'red')
Arrow('CIV1550', 8694.7 ,'red')

plt.subplot(Nx,Ny,6)
Arrow('SiII1526', 9143.2 ,'blue')
Arrow('SiII*1533', 9184.8,'blue')
Arrow('CIV1548', 9271.0 ,'blue')
Arrow('CIV1550', 9286.2 ,'blue')
Arrow('AlII1670', 9367,'red')

plt.subplot(Nx,Ny,7)
Arrow('Ni1710?', 9594.0 ,'red')
Arrow('FeII1608', 9633.5 ,'blue')
Arrow('FeII1611', 9649.5,'blue')

plt.subplot(Nx,Ny,8)
Arrow('AlII1670', 10006.0  ,'blue')

plt.subplot(Nx,Ny,9)
#10788       1801.0618050686 
Arrow('?', 10788  ,'blue')
Arrow('SiII1807', 10826  ,'blue')

  

plt.subplot(Nx,Ny,10)
Arrow('FeI1851?', 11078.0  ,'blue')
Arrow('AlIII1855', 11107.0  ,'blue')

plt.subplot(Nx,Ny,11)
Arrow('?', 11256.0  ,'blue')
  

plt.subplot(Nx,Ny,12)
Arrow('NiII*2217', 13280.0  ,'blue')
Arrow('NiII2222', 13314.0  ,'blue')
Arrow('?', 13361.0  ,'blue')
Arrow('FeII2250', 13475.0  ,'blue')


plt.subplot(Nx,Ny,13)
Arrow('FeII2586', 15491.0  ,'blue')
Arrow('FeII2599', 15570.0  ,'blue')




plt.subplot(Nx,Ny,14)
Arrow('MgII2796', 16744.0  ,'blue')
Arrow('MgII2803', 16787.0  ,'blue')

plt.subplot(Nx,Ny,15)
Arrow('MgII2852', 17087  ,'blue')


plt.show()