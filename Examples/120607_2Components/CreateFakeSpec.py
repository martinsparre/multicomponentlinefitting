import matplotlib.pyplot as plt
import scipy
import VoigtFitting.VoigtFitting as VoigtFitting
import time
import sys

#import AbsorptionLine, ResolutionConvolution,FitManager


SignalToNoise = float(sys.argv[1])
Resolution = float(sys.argv[2])
NComponents = int(sys.argv[3])

Fit = VoigtFitting.FitManager()

#read data:
#Fit.AddDataChunk('Data/NormVIS_Angstrom.txt', '9640A',9620.0, 9660.0,Resolution / 2.35482)

#define lines

Ion = Fit.CreateIonGroup('FeII',['FeII_1608','FeII_1611'])
Ion.AddComponent('A',2.0e15, 2500000.0 ,4.989)
if NComponents != 1:
    Ion.AddComponent('B',2.0e15, 1500000.0 ,4.99)


Wavelength = scipy.arange(9600.0,9700.0,0.005)
Flux = Fit.ComputeFlux(Wavelength)

#Instrument resolution:
Flux_convolve =  Fit.Convolve(Resolution/2.35482,Wavelength)

#Put on another grid:
Wavelength_obs = scipy.arange(9600.0,9700.0,0.25)
spline = scipy.interpolate.interp1d(Wavelength,Flux_convolve)
Flux_obs = spline(Wavelength_obs)

#Add gaussian noise
Flux_noise = Flux_obs + scipy.random.normal(0,SignalToNoise,len(Wavelength_obs))

z = 4.989

plt.plot(Wavelength_obs,Flux_noise,'-',color='grey',label='With noise',lw=2)
plt.plot(Wavelength,Flux,'-',color='black',label='Not convolved',lw=2)
plt.plot(Wavelength,Flux_convolve,'--',color='black',label='Convolved with resolution',lw=2)

#plt.plot(Fit.GetDataChunk('9640A').GetWavelength(),Fit.GetDataChunk('9640A').GetFlux(),'-',color='blue',label='Data',lw=1)
plt.xlim((9600,9700))
plt.ylim((-1,2))
plt.legend(loc=4)
plt.grid()
plt.xlabel('Wavelength [Angstrom]',fontsize=16)
plt.ylabel('Normalized flux',fontsize=16)
plt.show()

Output = file('120607_2Components/Data/SN' + sys.argv[1] + '_res'+ sys.argv[2] + '_NComponents'+sys.argv[3]+'.txt','w+')

for i in range(len(Wavelength_obs)):
    Output.write( str(Wavelength_obs[i]) + '\t' + str(Flux_noise[i]) + '\t' + str(SignalToNoise) + '\n' )

Output.close()