import matplotlib.pyplot as plt
import scipy
import VoigtFitting.VoigtFitting as VoigtFitting
import time,copy,sys
import LineDict.Pickle_functions as Pickle_functions
#import AbsorptionLine, ResolutionConvolution,FitManager


Fit = VoigtFitting.FitManager()

Data = sys.argv[1]
Resolution = float(sys.argv[2])
NComponents = int(sys.argv[3])

Fit.AddDataChunk(Data, '9640A',9620.0, 9660.0,Resolution / 2.35482)

#define lines
#Ion = Fit.CreateIonGroup('CIV',['CIV_1548','CIV_1550'])
#Ion.AddComponent('A',11.0e14, 3600000,4.9887)
#Ion.AddComponent('B',1e14,1000000,4.99)

Ion = Fit.CreateIonGroup('FeII',['FeII_1608','FeII_1611'])
Ion.AddComponent('A',2.0e15, 2500000.0 ,4.989)
if NComponents != 1:
    Ion.AddComponent('B',2.0e15, 1500000.0 ,4.99)

Fit.RunMCMC(1500)


b,N,z = Fit.GetFreeParameters()


plt.subplot(1,3,1)
plt.suptitle(Data,fontsize=10)
plt.xlabel(r'$b$ [km$/$s]')
plt.ylabel(r'$N$ [cm$^{-2}$]')
plt.plot(scipy.array(b[0].ListOfAcceptedValues)/100000.0,N[0].ListOfAcceptedValues,'-o',ms=2, mew=0,color='blue')
if NComponents != 1:
    plt.plot(scipy.array(b[1].ListOfAcceptedValues)/100000.0,N[1].ListOfAcceptedValues,'-o',ms=2, mew=0,color='red')

plt.grid()

plt.subplot(1,3,2)
plt.xlabel(r'$b$ [km$/$s]')
plt.hist(scipy.array(b[0].ListOfAcceptedValues)/100000.0, bins=40,color='blue')
if NComponents != 1:
    plt.hist(scipy.array(b[1].ListOfAcceptedValues)/100000.0, bins=40,color='red')
plt.grid()
plt.subplot(1,3,3)
plt.xlabel(r'$N$ [cm$^{-2}$]')
plt.hist(scipy.array(N[0].ListOfAcceptedValues), bins=40,color='blue')
if NComponents != 1:
    plt.hist(scipy.array(N[1].ListOfAcceptedValues), bins=40,color='red')
plt.grid()

plt.show()
Pickle_functions.SaveToRestartFile('tmp.fit',Fit)
Pickle_functions.SaveToRestartFile(Data.strip('.txt') + 'Fit_NC'+sys.argv[3]+'.fit',Fit)


Fit.RemovePoints()

