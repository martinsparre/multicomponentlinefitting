import matplotlib.pyplot as plt
import scipy,numpy,random
import VoigtFitting.VoigtFitting as VoigtFitting
import LineDict.Pickle_functions as Pickle_functions
import emcee
import sys

#import AbsorptionLine, ResolutionConvolution,FitManager

##############################
### Initialize FitManager  ###
##############################

Fit = VoigtFitting.FitManager()

#Fit.AddDataChunk('/home/ms/Uni/Observations/MCMCLineFitting/multicomponentlinefitting/EmceeTest/SII_spec.txt', '9640A',7503.0, 7518.0,0.25)
Fit.AddDataChunk('/home/ms/Uni/Observations/MCMCLineFitting/multicomponentlinefitting/EmceeTest/SII_1253.txt', '9640A',7503.0, 7518.0,0.362)
Fit.AddDataChunk('/home/ms/Uni/Observations/MCMCLineFitting/multicomponentlinefitting/EmceeTest/NiII_Norm.txt','8200A',8205.0, 8214.0,0.396)

#define lines
#Ion = Fit.CreateIonGroup('CIV',['CIV_1548','CIV_1550'])
#Ion.AddComponent('A',11.0e14, 3600000,4.9887)
#Ion.AddComponent('B',1e14,1000000,4.99)

Ion = Fit.CreateIonGroup('SII',['SII_1253'])
Ion.AddComponent('A',15.3, 2016000,4.989800)
Ion.AddComponent('B',15.3, 2030000,4.991488)


Ion = Fit.CreateIonGroup('NiII',['NiII_1370'])
Ion.AddComponent('A',14.3, 2016000,4.989800)
Ion.AddComponent('B',14.3, 2030000,4.991488)

#Ion.GetComponent('A').Get_z().Fix()
#Ion.GetComponent('B').Get_z().Fix()#make this work!

Fit.LinkParameters(['A','B'],['SII','NiII'],'zb')

Fit.CreateFreeParamDiagram(FilePrefix='Init')

print Fit.GetParameterArray()

sys.exit()
b,N,z = Fit.GetFreeParameters()

Length = len(b) + len(N) + len(z)

#ValArray,TypeArray = Fit.GetParameterArray()
#Fit.PlotChunks()
#plt.show()
#Fit.SetFreeParameters(ValArray)


#Fit.Link(['B'],['FeII','CIV'],'zb')

################################
## The function used by emcee ##
################################

def lnprob(ParameterArray,TypeArray,Limits,FitManager):
    #define priors:
    for i in range(len(TypeArray)):
        if TypeArray[i] in Limits.keys():
            LowerLimit = Limits[TypeArray[i]][0]
            UpperLimit = Limits[TypeArray[i]][1]

            if ParameterArray[i] > UpperLimit or ParameterArray[i] < LowerLimit:
                return -250.0
    
    #calc ln-likelihood
    FitManager.SetFreeParameters(ParameterArray)
    lnp = -Fit.CalcLnLikelihood()

    return lnp 


#regions excluded by prior
Limits = {}
Limits['b'] = [800000.0,4000000.0]
Limits['logN'] = [12.0,19.0]    
################################
#### Run emcee              ####
################################

ParamArray,TypeArray = Fit.GetParameterArray()
ndim = len(ParamArray)
nwalkers = 2*ndim

WalkerGuesses = Fit.CreateWalkerGuesses(0.5,0.7,0.00008,ndim,nwalkers)


sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, args=[TypeArray,Limits,Fit])


pos, prob, state = sampler.run_mcmc(WalkerGuesses, 2000)#burn-in

sampler.reset()
sampler.run_mcmc(pos, 6000)


Pickle_functions.SaveToRestartFile('MCMC_Output.pickle',[pos, prob, state,sampler.flatchain])

print TypeArray