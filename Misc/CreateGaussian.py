import matplotlib.pyplot as plt
import scipy
import VoigtFitting.VoigtFitting as VoigtFitting
import time
#import AbsorptionLine, ResolutionConvolution,FitManager

x = scipy.arange(-5,5,0.02)
y = VoigtFitting.Gauss(x,1.25 / 2.35482)
y /= scipy.integrate.trapz(y,x)
F = open('GaussInstrumentProfile.txt','w+')

for i in range(len(x)):
    F.write(str(x[i]) + '\t' + str(y[i]) + '\n')

F.close()