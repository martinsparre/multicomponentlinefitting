#!/usr/bin/python
# -*- coding: utf-8 -*-

import pickle

def SaveToRestartFile(PickleFile,Object):
    f=open(PickleFile, 'wb')
    pickle.dump(Object,f)
    f.close()

def LoadRestartFile(PickleFile):
    f=open(PickleFile, 'rb')
    Object = pickle.load(f)
    f.close()
    return Object
