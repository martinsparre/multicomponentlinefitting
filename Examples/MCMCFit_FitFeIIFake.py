import matplotlib.pyplot as plt
import scipy
import VoigtFitting.VoigtFitting as VoigtFitting
import time,copy,sys
import LineDict.Pickle_functions as Pickle_functions
#import AbsorptionLine, ResolutionConvolution,FitManager


Fit = VoigtFitting.FitManager()

Data = sys.argv[1]
Resolution = float(sys.argv[2])

Fit.AddDataChunk(Data, '9640A',9620.0, 9660.0,Resolution / 2.35482)

#define lines
#Ion = Fit.CreateIonGroup('CIV',['CIV_1548','CIV_1550'])
#Ion.AddComponent('A',11.0e14, 3600000,4.9887)
#Ion.AddComponent('B',1e14,1000000,4.99)

Ion = Fit.CreateIonGroup('FeII',['FeII_1608','FeII_1611'])
Ion.AddComponent('A',3.9e14, 1700000,4.989)
#Ion.AddComponent('B',3.9e13, 700000,4.9892)
#Ion.AddComponent('B',3.9e12, 17000,7.989)
#Ion.AddComponent('B',6.0e16, 300000,4.99)
#Ion.SetPrior_b('lognormal',Mu=20000,Sigma=30000)
#Ion.SetPrior_N('lognormal')
#Ion.SetPrior_z('gauss')

#Fit.LinkParameters(['A','B'],['FeII','CIV'],'z')
#Fit.Link(['B'],['FeII','CIV'],'zb')


#print Fit.CalcLnLikelihood()


#Fit.CreateFreeParamDiagram()


Fit.RunMCMC(5000)


b,N,z = Fit.GetFreeParameters()


plt.subplot(1,3,1)
plt.suptitle(Data,fontsize=10)
plt.xlabel(r'$b$ [km$/$s]')
plt.ylabel(r'$N$ [cm$^{-2}$]')
plt.plot(scipy.array(b[0].ListOfAcceptedValues)/100000.0,N[0].ListOfAcceptedValues,'-o',ms=2, mew=0,color='blue')



plt.plot(float(Data.split('_')[3].strip('b'))/100000.0,float(Data.split('_')[4].strip('N')),'x',ms=9, mew=3,color='red')
plt.grid()

plt.subplot(1,3,2)
plt.xlabel(r'$b$ [km$/$s]')
plt.hist(scipy.array(b[0].ListOfAcceptedValues)/100000.0, bins=40)
plt.grid()
plt.subplot(1,3,3)
plt.xlabel(r'$N$ [cm$^{-2}$]')
plt.hist(scipy.array(N[0].ListOfAcceptedValues), bins=40)
plt.grid()

plt.show()

Pickle_functions.SaveToRestartFile('thisisatest1.fit',Fit)
